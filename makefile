ASM=nasm
ASMFLAGS=-felf64
LD=ld
lab2: out/main.o out/dict.o out/lib.o
	$(LD) -o $@ $^
out/%.o: src/%.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

out/main.o: src/main.asm src/lib.inc src/words.inc src/colon.inc
	$(ASM) $(ASMFLAGS) -o $@ $<
.PHONY: run clean
clean:
	$(RM) out/* lab2
run:
	./lab2
