section .data
%define EXIT_CODE 60
%define WRITE_CODE 1
%define STDOUT_DESCRIPTOR 1
%define CHAR_LENGTH 1
%define NEWLINE_CODE 10
%define DIVIDER 10
%define ALLOCATED_MEMORY 21
%define OFFSET 0x30
%define MINUS_CODE 0x2D
%define WHITESPACE_CODE 0x20
%define TAB_CODE 0x9

global exit
global string_length
global print_string
global print_newline
global string_equals
global string_copy
global read_word
global read_line
global print_stderr

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi + rax], 0
	je .END
	inc rax
	jmp .loop
    .END:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi; save caller-save register before call
    call string_length
    pop rdi; restore register
    mov rdx, rax; string length
    mov rax, WRITE_CODE; syscall number
    mov rsi, rdi; write string pointer
    mov rdi, STDOUT_DESCRIPTOR; file descriptor
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CODE 

print_char:
    push rdi
    mov rdi, rsp
    mov rax, WRITE_CODE
    mov rsi, rdi
    mov rdi, STDOUT_DESCRIPTOR
    mov rdx, CHAR_LENGTH
    syscall
    pop rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jl .NEG
    jmp print_uint
    .NEG:
        neg rdi
	push rdi
	mov rdi, MINUS_CODE
	call print_char
	pop rdi	

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rdi, DIVIDER
    mov rsi, rsp
    sub rsp, ALLOCATED_MEMORY
    dec rsi
    mov byte [rsi], 0
    dec rsi
    .loop:
        mov rdx, 0
        div rdi
        add rdx, OFFSET
        mov [rsi], dl
        dec rsi
        test rax, rax
        je .PRINT
        jmp .loop
    .PRINT:
        inc rsi
        mov rdi, rsi
        call print_string
    add rsp, ALLOCATED_MEMORY
    ret

;rdi - message
print_stderr:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    call string_length
    pop rdi
    push rax; save length first string
    push rdi
    push rsi
    mov rdi, rsi
    call string_length
    pop rsi
    pop rdi; restore two pointers after computing second length
    pop r11; here is first length, second is in rax
    cmp rax, r11
    jne .FALSE; if we go further we know length1 == length2
    cmp rax, 0
    je .TRUE
    .loop:
        mov r11b, [rdi]
        cmp r11b, [rsi]
	jne .FALSE
	inc rdi
	inc rsi
        dec rax
	jne .loop
    .TRUE:
        mov rax, 1
	jmp .END
    .FALSE:
        xor rax, rax
    .END:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, CHAR_LENGTH
    dec rsp
    mov rsi, rsp
    syscall
    test rax, rax
    je .EOF
    mov al, [rsp]
    .EOF:
        inc rsp
        ret 

read_line:
    push rdi
    push rdi
    push rsi
    .A:
        call read_char
	cmp rax, WHITESPACE_CODE 
	je .A
	cmp rax, TAB_CODE
	je .A
	cmp rax, NEWLINE_CODE
	je .A
    ; restore registers, rax - cur symbol
    .B:
        pop rsi
	pop rdi
        test rsi, rsi
	je .WRONG
	mov [rdi], al
	test rax, rax
	je .RIGHT
	cmp rax, NEWLINE_CODE
	je .RIGHT
	inc rdi
	dec rsi
	push rdi
	push rsi
	call read_char
	jmp .B
    .RIGHT:
        mov byte [rdi], 0
        mov rdi, [rsp]
        call string_length
	mov rdx, rax
	pop rax
	jmp .END
    .WRONG:
        pop rax
        xor rax, rax
    .END:
        ret	

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax; number
    xor rsi, rsi; cur_char, length
    xor rdx, rdx; 0 for mul
    mov r11, DIVIDER
    .A:
        push rsi
        mov sil, [rdi]
	test sil, sil
	je .END
	cmp sil, NEWLINE_CODE
	je .END
	cmp sil, '0'
	jb .END
	cmp sil, '9'
	ja .END
        sub sil, OFFSET
	mul r11
	add rax, rsi
	inc rdi
	pop rsi
	inc rsi
	jmp .A
    .END:
        pop rdx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; try parse_uint
; if rdx == 0 then read_char (to get sign)
; parse_uint
; profit
parse_int:
    push rdi; save pointer
    call parse_uint
    pop rdi
    test rdx, rdx
    jne .END; already parsed
    mov sil, [rdi]; here is the sign, it will come later
    inc rdi
    push rsi
    call parse_uint
    inc rdx
    pop rsi
    cmp sil, '-'
    jne .END
    neg rax 
    .END:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; rdi - string pointer
; rsi - buffer pointer
; rdx - buffer length
string_copy:
    push rdx
    push rsi
    push rdi
    call string_length; rax - length of the string
    inc rax; add 1 because of the null-termination
    pop rdi
    pop rsi
    pop rdx
    cmp rax, rdx
    jg .WRONG_LENGTH
    push rax
    .loop:
        mov r11b, [rdi]
	mov [rsi], r11b
	inc rdi
	inc rsi
        dec rax
	jne .loop
    pop rax
    jmp .END
    .WRONG_LENGTH:
        xor rax, rax
    .END:
        ret
