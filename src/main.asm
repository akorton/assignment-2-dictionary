%include "src/words.inc"

section .rodata
%define buffer_capacity 256
key: db "third word", 0
error_msg: db "No such key", 10, 0
no_input_msg: db "Nothing was inputed", 10, 0

section .data
buffer: times 4 dq 0

section .text
%include "src/lib.inc"
global _start
extern find_word
_start:
	mov rdi, buffer
	mov rsi, buffer_capacity
	call read_line
	test rdx, rdx
	je .noinput
	mov rdi, rax 
	mov rsi, POINTER
	call find_word
	test rax, rax
	je .notfound
	mov rdi, rax
	call print_string
	call print_newline
	jmp .exit
	.notfound:
	mov rdi, error_msg
	call print_stderr
	jmp .exit
	.noinput:
	mov rdi, no_input_msg
	call print_stderr
	.exit:
	xor rdi, rdi
	call exit
	
