%define POINTER 0

%macro colon 2
%ifid %2
	%2:
	dq POINTER
%else
	%fatal "value must be id"
%endif
%ifstr %1
	db %1, 0
%else 
	%fatal "value must be string"
%endif
%define POINTER %2
%endmacro
