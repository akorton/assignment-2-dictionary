%include "src/lib.inc"
section .rodata
%define KEY_LENGTH 8
section .text
global find_word

find_word:
	;rdi - key
	;rsi - first elem
	.loop:
	test rsi, rsi
	je .notfound
	push rsi
	add rsi, KEY_LENGTH
	push rdi
	call string_equals
	test rax, rax
	jne .found
	pop rdi
	pop rsi
	mov rsi, [rsi]
	jmp .loop
	.found:
	pop rdi
	pop rax
	add rax, KEY_LENGTH; get past next element
	push rax
	mov rdi, rax
	call string_length
	add rax, [rsp]
	inc rax
	pop rdi 
	ret	
	.notfound:
	xor rax, rax
	ret

